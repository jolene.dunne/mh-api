from rest_framework import viewsets

from .serializers import ActivitySerializer, ProgramSerializer, SectionSerializer
from .models import Activity, Program, Section

# I didn't see any specification for what should be queryable, so these are
# just some example views that I thought made sense.


class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all().order_by('id')
    serializer_class = ActivitySerializer


class ProgramViewSet(viewsets.ModelViewSet):
    queryset = Program.objects.all().order_by('name')
    serializer_class = ProgramSerializer


class SectionViewSet(viewsets.ModelViewSet):
    queryset = Section.objects.all().order_by('name')
    serializer_class = SectionSerializer
