from django.contrib import admin
from rest_framework import routers
from . import views
from django.urls import path, include

router = routers.DefaultRouter()
router.register(r'program', views.ProgramViewSet)
router.register(r'section', views.SectionViewSet)
router.register(r'activity', views.ActivityViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls)
]
