from django.db import models

# General notes - I explicitly declared the id field just as personal preference
# I modelled the relationships between objects as many-to-one, explained in more detail below
# Everything else I think is standard format for what was requested in the brief, or
# deliberately chosen to reduce complexity in the model.


class Program(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField()


class Section(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255)
    description = models.TextField()
    overview_image = models.ImageField(null=True, blank=True)
    # I wasn't 100% sure what this field represented - is it the order that these sections
    # should appear in the program? If so, I think an integer field is sufficient for this
    order_index = models.IntegerField()
    # Similar to the Activity model, my assumption here is that sections are specific to a
    # program, and that a section would not appear in multiple different programs. If that is
    # incorrect then I would change this to a many-to-many field.
    program = models.ForeignKey(Program, related_name='sections', on_delete=models.CASCADE, null=True)


class Activity(models.Model):
    id = models.BigAutoField(primary_key=True)
    # I chose to represent the presence of the HTML content or the question
    # as a generic content field - I figured that the question could be represented as
    # some markup
    content = models.TextField()
    # My assumption here is that the relationship between activities and sections is a many to one
    # foreign key relationship - I thought that certain activities will be associated with
    # a single section. If this is not the case I would change this to a many-to-many field.
    section = models.ForeignKey(Section, related_name='activities', on_delete=models.CASCADE, null=True)

