# Modern Health API Assignment

## Requirements
Python3 and pip installed

## Setup
On MacOS, in directory of cloned repository - 
```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Run
```
cd modern_health
python manage.py migrate
python ./manage.py runserver
```

Server will run at http://127.0.0.1:8000/ - can add and retrieve entries via curl or UI

## General Notes
I have commented any files that weren't Django boilerplate with reasoning behind the implementation. Note that this is probably more than I would usually write for comments
as I don't think a codebase is the correct place for explanations of design decisions

## Tests
I did not include tests as I'm not particularly familiar with how to set up Django-specific tests
for the endpoints. If I had more time I would have added tests covering the standard REST operations on each of the available
models.