from django.contrib import admin
from .models import Activity, Section, Program

admin.site.register(Activity)
admin.site.register(Section)
admin.site.register(Program)
