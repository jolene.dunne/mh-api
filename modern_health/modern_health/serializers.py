from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer
from .models import Activity, Program, Section


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id', 'content')


class SectionSerializer(serializers.ModelSerializer):
    activities = ActivitySerializer(many=True)

    class Meta:
        model = Section
        fields = ('id', 'name', 'description', 'overview_image', 'order_index', 'activities')
        depth = 2


class ProgramSerializer(WritableNestedModelSerializer, serializers.ModelSerializer):
    sections = SectionSerializer(many=True)

    class Meta:
        model = Program
        fields = ('id', 'name', 'description', 'sections')
        depth = 2


